# Incube

Kill the bugs and catch the squirrels before the week runs out.

## Gameplay

Press spacebar to shoot at the bugs, a hit yields 3 points
Catching a squirrel (wasd) yields 10 points.

## Building

If on Windows or Linux with Xorg:

`$ go build incube.go`

If on Linux with Wayland:

```
# apt install xwayland xorg-dev
$ go build incube.go
```
