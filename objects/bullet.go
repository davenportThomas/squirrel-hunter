package objects

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"image/color"
)

type Bullet struct {
	X1        float64
	Y1        float64
	X2        float64
	Y2        float64
	direction bool // false for left, true for right
}

// start the bullet off
func (b *Bullet) Fire(x1 float64, y1 float64, x2 float64, y2 float64, direction bool) {
	//bullet is firing, so return
	if b.X1 > 0 {
		return
	}
	b.X1 = x1
	b.Y1 = y1
	b.X2 = x2 + 6
	b.Y2 = y2
	b.direction = direction
}

// bulletState checks the current state of the bullet
func (b *Bullet) BulletState() {
	var speed float64
	speed = 8
	//if the bullet is still visible, push it forward
	if b.X1 > 0 && b.X1 < 330 {
		//move bullet forward
		b.X1 = b.X1 + speed
		b.X2 = b.X2 + speed
	} else { //reset the bullet
		b.X1 = -20
		b.X2 = -20
	}
}

func (b *Bullet) Draw(screen *ebiten.Image) {
	ebitenutil.DrawLine(screen, b.X1, b.Y1, b.X2, b.Y2, color.NRGBA{0, 255, 0, 255})
}
