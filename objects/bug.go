package objects

import (
	"fmt"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"image/color"
	"math/rand"
)

// ticks is updated every, tick, used for death/life reset currently
var ticks uint

type Bug struct {
	X1        float64
	Y1        float64
	Width     float64
	Height    float64
	direction bool //false for down, true for up
	alive     bool
	Color     color.NRGBA
}

// Init creates a bug in a random location, within bounds of the bug pit of course
func (b *Bug) Init() {
	randy := float64(rand.Int()%100 + 40)
	randx := float64(rand.Int()%150 + 120)
	b.X1 = randx
	b.Y1 = randy
	b.Width = 5
	b.Height = 5
	if int(randy)%2 == 0 {
		b.direction = false
	} else {
		b.direction = true
	}
	b.alive = true
	ticks = 0
	b.Color = color.NRGBA{255, 255, 255, 255}
}

// CollisionCheck if a bullet has smacked into a bug
func (b *Bug) CollisionCheck(bullet Bullet) bool {
	//check x position
	if bullet.X1 > (b.X1-5) && bullet.X1 < (b.X1+5) {
		//check y position
		if bullet.Y1 > b.Y1-5 && bullet.Y1 < b.Y1+10 {
			fmt.Println("BOOM")
			b.alive = false
			return true
		}
	}
	return false
}

// Move determines the bugs next move
func (b *Bug) Move() {
	if b.alive == false {
		b.death()
		return
	}
	//vertical movement
	if b.Y1 < 20 || b.Y1 > 220 {
		b.swapDirection()
	}
	if b.direction {
		b.moveUp()
	} else {
		b.moveDown()
	}
}

// swapDirection does exactly that
func (b *Bug) swapDirection() {
	b.direction = !b.direction
}

func (b *Bug) moveUp() {
	b.Y1 = b.Y1 + 1.5
}

func (b *Bug) moveDown() {
	b.Y1 = b.Y1 - 1.5
}

// death for when a bug is in the death state, ensure it stays hidden
func (b *Bug) death() {
	ticks++
	b.X1 = -1
	b.Y1 = -1
	b.Width = 0
	b.Height = 0
	//revive the bug and reset ticks
	if ticks > 200 {
		ticks = 0
		b.alive = false
		b.Init()
	}
}

func (b *Bug) Draw(screen *ebiten.Image) {
	//bug body
	ebitenutil.DrawRect(screen, b.X1, b.Y1, b.Width, b.Height, b.Color)
	//bug legs
	if b.X1 > 0 {
		ebitenutil.DrawLine(screen, b.X1-2, b.Y1+1, b.X1+7, b.Y1+1, b.Color)
		ebitenutil.DrawLine(screen, b.X1-2, b.Y1+4, b.X1+7, b.Y1+4, b.Color)
	}
}
