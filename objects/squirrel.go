package objects

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"image/color"
	"math/rand"
)

type Squirrel struct {
	X1    float64
	Y1    float64
	Size  float64
	alive bool
	Color color.NRGBA
	speed float64
}

func (s *Squirrel) Init() {
	randy := float64(rand.Int()%200 + 20)
	s.X1 = 500
	s.Y1 = randy
	s.Size = 5
	s.Color = color.NRGBA{255, 80, 31, 255}
	s.speed = 3
}

func (s *Squirrel) Move() {
	if s.X1 < 0 {
		s.Init()
	}
	s.X1 = s.X1 - s.speed
}

func (s *Squirrel) CollisionCheck(player Plr) bool {
	//check x
	if s.X1 >= (player.X1-s.speed) && (s.X1 <= player.X2+s.speed) {
		//check y
		if s.Y1 > (player.Y1-s.Size) && s.Y1 < player.Y2 {
			s.Init()
			return true
		}
	}
	return false
}

func (s Squirrel) Draw(screen *ebiten.Image) {
	//body
	ebitenutil.DrawRect(screen, s.X1, s.Y1, s.Size+3, s.Size, s.Color)
	//tail
	ebitenutil.DrawRect(screen, (s.X1 + s.Size), s.Y1+(s.Size/2), s.Size*2, 2, s.Color)
}
