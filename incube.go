package main

import (
	"drawing/objects"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"image/color"
	"log"
	"strconv"
	"time"
)

type Game struct {
	keys []ebiten.Key
}

var character objects.Plr
var bullets [2]objects.Bullet
var bugs []objects.Bug
var squirrel objects.Squirrel
var score int
var start bool
var ticks int64
var gameOver bool
var bulletCool int

// if spot is available, make a new bullet
func createBullet() {
	for i, bullet := range bullets {
		if bullet.X1 < 0.0 && bulletCool > 20 {
			b := new(objects.Bullet)
			b.Fire(character.X1, character.Y1+4, character.X2+4, character.Y2-6, true)
			bullets[i] = *b
			bulletCool = 0
			return
		}
	}
}

// game controls come in here
func controls(g *Game) {
	//putting these controls in a loop makes them work smoothly
	g.keys = inpututil.AppendPressedKeys(g.keys[:0])
	for _, key := range g.keys {
		switch dir := key; dir {
		case ebiten.KeyD:
			character.MoveRight()
		case ebiten.KeyA:
			character.MoveLeft()
		case ebiten.KeyW:
			character.MoveUp()
		case ebiten.KeyS:
			character.MoveDown()
		}
	}
	if inpututil.IsKeyJustPressed(ebiten.KeySpace) {
		if start == false {
			start = true
			time.Sleep(500 * time.Millisecond)
		} else if gameOver != true {
			createBullet()
		}
	}
	//reset the game
	if inpututil.IsKeyJustPressed(ebiten.KeyR) {
		start = false
		character.X1 = 0
		character.X2 = 0
	}
}

func gameInit() {
	if character.X1 == 0 && start == true {
		character.Init()
		squirrel.Init()
		bug := new(objects.Bug)
		//delete any old bugs
		bugs = nil
		// make new bugs
		bugs = append(bugs, *bug)
		bugs = append(bugs, *bug)
		bugs = append(bugs, *bug)
		score = 0
		gameOver = false
		start = true
		ticks = 1
		bulletCool = 0
	}
}

func updateBug() {
	//add a new bug
	if ticks%100 == 0 && gameOver == false {
		bug := new(objects.Bug)
		bugs = append(bugs, *bug)
	}
	//check the bug state
	for i := 0; i < len(bugs); i++ {
		if bugs[i].X1 == 0 {
			bugs[i].Init()
		}
		bugs[i].Move()
	}
}

func updateBullets() {
	//check the bullets state
	for i := 0; i < len(bullets); i++ {
		bullets[i].BulletState()
		for j := 0; j < len(bugs); j++ { //TODO: gotta do better than O(N^2)
			if bugs[j].CollisionCheck(bullets[i]) {
				if gameOver == false && start == true {
					score = score + 3
				}
			}
		}
	}
}

func updateSquirrel() {
	squirrel.Move()
	if squirrel.CollisionCheck(character) {
		if gameOver == false && start == true {
			score = score + 10
		}
	}
}

// where the game tries to update at 60hz
func (g *Game) Update() error {
	bulletCool++
	if start == true {
		ticks++
	}
	//initial game init
	gameInit()

	//check the bugs
	updateBug()

	//check the bullets
	updateBullets()

	//check the squirrels state
	updateSquirrel()

	//update the controls
	controls(g)

	//game over
	if ticks == 3600 {
		gameOver = true
	}

	return nil
}

func introScreen(screen *ebiten.Image) {
	//title
	ebitenutil.DebugPrintAt(screen, "Incube: The Squirrel Chaser", 10, 10)
	ebitenutil.DebugPrintAt(screen, "Kill the bugs       Catch the squirrel", 10, 40)
	//draw preview bug, TODO: render this with a function
	ebitenutil.DrawRect(screen, 20, 72, 5, 5, color.NRGBA{255, 255, 255, 255})
	ebitenutil.DrawLine(screen, 18, 73, 27, 73, color.NRGBA{255, 255, 255, 255})
	ebitenutil.DrawLine(screen, 18, 76, 27, 76, color.NRGBA{255, 255, 255, 255})
	ebitenutil.DebugPrintAt(screen, "x 3 point", 30, 65)

	//draw preview squirrel
	ebitenutil.DrawRect(screen, 140, 72, 8, 5, color.NRGBA{255, 80, 31, 255})
	ebitenutil.DrawRect(screen, (145), 75, 10, 2, color.NRGBA{255, 80, 31, 255})
	//ebitenutil.DrawRect(screen, 150, 72, 5, 5, color.NRGBA{255, 80, 31, 255})
	ebitenutil.DebugPrintAt(screen, "x 10 points", 160, 65)

	//draw instructions
	ebitenutil.DebugPrintAt(screen, "WASD to move, SPACE to fire, R to restart", 10, 90)
	ebitenutil.DebugPrintAt(screen, "Press SPACE to start", 10, 200)
}

func playScreen(screen *ebiten.Image) {
	//background
	screen.Fill(color.NRGBA{0, 0, 120, 200})
	//for the squirrel
	squirrel.Draw(screen)
	//bug pit
	ebitenutil.DrawRect(screen, 100, 10, 215, 220, color.Black)
	//for the character
	character.Draw(screen)
	//for the bullets
	for _, bullet := range bullets {
		bullet.Draw(screen)
	}
	//lets draw bugs
	for _, bug := range bugs {
		bug.Draw(screen)
	}
	//the scoreboard
	var scoreStr string
	scoreStr = "Jira Points: " + strconv.Itoa(score)
	ebitenutil.DebugPrintAt(screen, scoreStr, 220, 10)

	//time remaining => 3600 ticks is roughly 1 minute,3600/7 is rougly 510, 1 is offset
	timeLeft := (3600-int(ticks))/(510) + 1
	timeStr := "Days in sprint: " + strconv.Itoa(timeLeft)
	ebitenutil.DebugPrintAt(screen, timeStr, 105, 10)
}

func gameOverScreen(screen *ebiten.Image) {
	finalScore := "Final score: " + strconv.Itoa(score)
	ebitenutil.DebugPrintAt(screen, "GAME OVER", 10, 10)
	ebitenutil.DebugPrintAt(screen, finalScore, 10, 70)
	ebitenutil.DebugPrintAt(screen, "Press R to restart", 10, 100)
}

// objects to draw go in here
func (g *Game) Draw(screen *ebiten.Image) {

	if start == false {
		introScreen(screen)
		return
	}
	if gameOver == true {
		gameOverScreen(screen)
		return
	}
	playScreen(screen)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return 320, 240
}

func main() {
	start = false
	ticks = -1
	ebiten.SetWindowSize(640, 480)
	ebiten.SetWindowTitle("Incube")
	if err := ebiten.RunGame(&Game{}); err != nil {
		log.Fatal(err)
	}
}
